import UIKit

protocol BerlinClockProtocol {
    // return the part of time that needs to be individually parsed by the different Parsers
    func extract(digitalTime: String) -> String
    func extract(berlinTime: String) -> String
    
    // convert a berlin time <-> digital time
    func berlinTime(from: String) -> String
    func digitalTimeInSeconds(from: String) -> Int
    
    // helper to get the number of activated lamp for a given time (hours, minutes or seconds)
    func numberOfActivatedLampsFromDigitalTime(from digitalTime: String) -> Int
}

enum Time: Int {
    case hours = 0
    case minutes = 1
    case seconds = 2
}

class Utils {
    static func secondsToHoursMinutesSeconds(seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    static func validate(digitalTime: String) -> Bool {
        let parts = digitalTime.split(separator: ":")
        guard parts.count == 3 else {
            return false
        }
        
        let hours = parts[0]
        let minutes = parts[1]
        let seconds = parts[2]
        
        guard hours.count == 2, minutes.count == 2, seconds.count == 2 else {
            return false
        }
        
        // Check if time contains numbers only
        let times = [hours, minutes, seconds]
        let digits = CharacterSet.decimalDigits
        for time in times {
            for c in time.unicodeScalars {
                if !digits.contains(c) {
                    return false
                }
            }
        }
        
        return true
    }
    
    static func validate(berlinTime: String) -> Bool {
        guard berlinTime.count == 24 else {
            return false
        }
        
        let allowedCharacters = ["O", "R", "Y"]
        for c in berlinTime {
            if !allowedCharacters.contains(String(c)) {
                return false
            }
        }
        return true
    }
}

class BerlinClock {
    let clocks: [BerlinClockProtocol] = [Seconds(), FiveHours(), SingleHours(), FiveMinutes(), SingleMinutes()]
    
    func berlinTime(from: String) -> String {
        guard Utils.validate(digitalTime: from) else {
            return ""
        }
        
        // Construct the berlin time clock by clock (i.e row by row)
        var time = ""
        for clock in clocks {
            let result = clock.berlinTime(from: from)
            time.append(result)
        }
        
        return time
    }
    
    func digitalTime(from: String) -> String {
        guard Utils.validate(berlinTime: from) else {
            return ""
        }
        
        // Add seconds, minutes and hours
        var time = 0
        for clock in clocks {
            time += clock.digitalTimeInSeconds(from: from)
        }
        
        let (hours, minutes, seconds) = Utils.secondsToHoursMinutesSeconds(seconds: time)
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
}

class BaseClockParser: BerlinClockProtocol {
    var typeOfTime = Time.seconds
    var numberOfLampsInRow = 0
    
    func berlinTime(from: String) -> String {
        // Extract the hours, minutes or seconds from the time
        let digitalPart = self.extract(digitalTime: from)
        // Get the corresponding number of lamps
        let numberOfActivatedLamps = self.numberOfActivatedLampsFromDigitalTime(from: digitalPart)
        // Convert the number of lamps to a proper string representation
        return self.berlinTimeFrom(numberOfActivatedLamps: numberOfActivatedLamps)
    }
    
    func digitalTimeInSeconds(from: String) -> Int {
        return self.numberOfActivatedLampsFromDigitalTime(from: from) * 60
    }
    
    func extract(digitalTime: String) -> String {
        // HH:MM:SS
        let parts = digitalTime.split(separator: ":")
        guard parts.count == 3 else {
            return ""
        }
        
        return String(parts[typeOfTime.rawValue])
    }
    
    func numberOfActivatedLampsFromDigitalTime(from digitalTime: String) -> Int {
        guard let numberOfActivatedLamps = Int(digitalTime) else {
            return 0
        }
        
        return numberOfActivatedLamps % 5
    }
    
    func berlinTimeFrom(numberOfActivatedLamps: Int) -> String {
        var result = ""
        var i = 0
        
        while ( i < self.numberOfLampsInRow ) {
            if i < numberOfActivatedLamps {
                result.append("Y")
            } else {
                result.append("O")
            }
            i = i + 1
        }
        
        return result
    }
    
    func extract(berlinTime: String) -> String {
        return ""
    }
    
    func numberOfActivatedLamps(from berlinTime: String) -> Int {
        let extractedPart = self.extract(berlinTime: berlinTime)
        return extractedPart.reduce(0) {
            ($1 == "Y" || $1 ==  "R") ? $0 + 1 : $0
        }
    }
}

class SingleMinutes: BaseClockParser {
    override init() {
        super.init()
        self.typeOfTime = Time.minutes
        self.numberOfLampsInRow = 4
    }
    
    // Last 4 characters
    override func extract(berlinTime: String) -> String {
        let last = berlinTime.dropFirst(20)
        return String(last)
    }
    
    override func digitalTimeInSeconds(from: String) -> Int {
        return numberOfActivatedLamps(from: from) * 1 * 60
    }
}

class FiveMinutes: BaseClockParser {
    
    override init() {
        super.init()
        self.typeOfTime = Time.minutes
        self.numberOfLampsInRow = 11
    }
    
    override func numberOfActivatedLampsFromDigitalTime(from digitalTime: String) -> Int {
        guard let numberOfActivatedLamps = Int(digitalTime) else {
            return 0
        }
        
        return numberOfActivatedLamps / 5
    }
    
    override func berlinTimeFrom(numberOfActivatedLamps: Int) -> String {
        var result = ""
        var i = 0
        
        while ( i < self.numberOfLampsInRow ) {
            if i < numberOfActivatedLamps {
                var letterToAppend = "Y"
                if (i + 1) % 3 == 0 {
                    letterToAppend = "R"
                }
                result.append(letterToAppend)
            } else {
                result.append("O")
            }
            i = i + 1
        }
        
        return result
    }
    
    // 11 characters after the seconds/hours lamps
    override func extract(berlinTime: String) -> String {
        let last = berlinTime.dropFirst(9).dropLast(4)
        return String(last)
    }
    
    override func digitalTimeInSeconds(from: String) -> Int {
        return numberOfActivatedLamps(from: from) * 5 * 60
    }
}

class SingleHours: BaseClockParser {
    override init() {
        super.init()
        self.typeOfTime = Time.hours
        self.numberOfLampsInRow = 4
    }
    
    override func numberOfActivatedLampsFromDigitalTime(from digitalTime: String) -> Int {
        guard let numberOfActivatedLamps = Int(digitalTime) else {
            return 0
        }
        
        return numberOfActivatedLamps % 5
    }
    
    override func berlinTimeFrom(numberOfActivatedLamps: Int) -> String {
        var result = ""
        var i = 0
        
        while ( i < self.numberOfLampsInRow ) {
            if i < numberOfActivatedLamps {
                result.append("R")
            } else {
                result.append("O")
            }
            i = i + 1
        }
        
        return result
    }
    
    // 4 characters after the seconds/ 5 hours block lamps
    override func extract(berlinTime: String) -> String {
        let last = berlinTime.dropFirst(5).dropLast(15)
        return String(last)
    }
    
    override func digitalTimeInSeconds(from: String) -> Int {
        return numberOfActivatedLamps(from: from) * 1 * 60 * 60
    }
}

class FiveHours: BaseClockParser {
    override init() {
        super.init()
        self.typeOfTime = Time.hours
        self.numberOfLampsInRow = 4
    }
    
    override func numberOfActivatedLampsFromDigitalTime(from digitalTime: String) -> Int {
        guard let numberOfActivatedLamps = Int(digitalTime) else {
            return 0
        }
        
        return numberOfActivatedLamps / 5
    }
    
    override func berlinTimeFrom(numberOfActivatedLamps: Int) -> String {
        var result = ""
        var i = 0
        
        while ( i < self.numberOfLampsInRow ) {
            if i < numberOfActivatedLamps {
                result.append("R")
            } else {
                result.append("O")
            }
            i = i + 1
        }
        
        return result
    }
    
    // 4 characters after the seconds
    override func extract(berlinTime: String) -> String {
        let last = berlinTime.dropFirst(1).dropLast(19)
        return String(last)
    }
    
    override func digitalTimeInSeconds(from: String) -> Int {
        return numberOfActivatedLamps(from: from) * 5 * 60 * 60
    }
}

class Seconds: BaseClockParser {
    override init() {
        super.init()
        self.typeOfTime = Time.seconds
        self.numberOfLampsInRow = 1
    }
    
    override func numberOfActivatedLampsFromDigitalTime(from digitalTime: String) -> Int {
        guard let numberOfActivatedLamps = Int(digitalTime) else {
            return 0
        }
        
        return numberOfActivatedLamps % 2 // always return 0 or 1
    }
    
    override func berlinTimeFrom(numberOfActivatedLamps: Int) -> String {
        let result = numberOfActivatedLamps == 0 ? "Y" : "O"
        
        return result
    }
    
    // first characher
    override func extract(berlinTime: String) -> String {
        let last = berlinTime.dropLast(23)
        return String(last)
    }
    
    // We can only tell if the seconds are odd or even, here 59 and 0 are chosen arbitrarily
    override func digitalTimeInSeconds(from: String) -> Int {
        let lamp = numberOfActivatedLamps(from: from)
        return (lamp == 0) ? 59 : 0
    }
}

// To Berlin Time

let singleMinutes = SingleMinutes()

singleMinutes.berlinTime(from: "00:00:00")
singleMinutes.berlinTime(from: "23:59:59")
singleMinutes.berlinTime(from: "12:32:00")
singleMinutes.berlinTime(from: "12:34:00")
singleMinutes.berlinTime(from: "12:35:00")

let fiveMinutes = FiveMinutes()

fiveMinutes.berlinTime(from: "00:00:00")
fiveMinutes.berlinTime(from: "23:59:59")
fiveMinutes.berlinTime(from: "12:04:00")
fiveMinutes.berlinTime(from: "12:23:00")
fiveMinutes.berlinTime(from: "12:35:00")

let singleHours = SingleHours()

singleHours.berlinTime(from: "00:00:00")
singleHours.berlinTime(from: "23:59:59")
singleHours.berlinTime(from: "02:04:00")
singleHours.berlinTime(from: "08:23:00")
singleHours.berlinTime(from: "14:35:00")

let fiveHours = FiveHours()

fiveHours.berlinTime(from: "00:00:00")
fiveHours.berlinTime(from: "23:59:59")
fiveHours.berlinTime(from: "02:04:00")
fiveHours.berlinTime(from: "08:23:00")
fiveHours.berlinTime(from: "16:35:00")

let seconds = Seconds()

seconds.berlinTime(from: "00:00:00")
seconds.berlinTime(from: "23:59:59")

let berlinClock = BerlinClock()

berlinClock.berlinTime(from: "00:00:00")
berlinClock.berlinTime(from: "23:59:59")
berlinClock.berlinTime(from: "16:50:06")
berlinClock.berlinTime(from: "11:37:01")

// To Digital Time

berlinClock.digitalTime(from: "YOOOOOOOOOOOOOOOOOOOOOOO")
berlinClock.digitalTime(from: "ORRRRRRROYYRYYRYYRYYYYYY")
berlinClock.digitalTime(from: "YRRROROOOYYRYYRYYRYOOOOO")
berlinClock.digitalTime(from: "ORROOROOOYYRYYRYOOOOYYOO")
